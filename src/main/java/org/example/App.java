package org.example;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();

        loader.setLocation(getClass().getResource("/fxml/main.fxml"));

        Scene scene = new Scene(loader.load());

        primaryStage.setTitle("FileOrganizer");
        primaryStage.setX(50);
        primaryStage.setY(50);
        primaryStage.setWidth(600);
        primaryStage.setMinWidth(600);
        primaryStage.setHeight(480);
        primaryStage.setMinHeight(480);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

}
