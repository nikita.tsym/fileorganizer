package org.example;

import java.lang.reflect.InvocationTargetException;

public class RunnerFromMaven {
    public static void main(String[] args) {
        try {
//            String[] params = Arrays.copyOfRange(args, 1, args.length);
            String[] params = {"org.example.App"};
            Class.forName(params[0]).getMethod("main", String[].class).invoke(null, (Object) params);
        } catch (IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | NoSuchMethodException
                | SecurityException | ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
