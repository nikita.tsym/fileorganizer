package org.example;

import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

public class Controller implements Initializable {

    @FXML
    public MenuBar menuBar;
    @FXML
    public Button addDirButton;
    @FXML
    public ListView<File> dirList;
    @FXML
    public Button scanButton;
    @FXML
    public Button removeDirButton;
    @FXML
    public Button stopButton;
    @FXML
    public Label progressLabel;
    @FXML
    public ProgressBar scanningProgressBar;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        scanButton.setDisable(true);
        removeDirButton.setDisable(true);

        addDirButton.setOnAction(actionEvent -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File file = directoryChooser.showDialog(addDirButton.getScene().getWindow());
            if (file != null) {
                if (!dirList.getItems().contains(file)) {
                    dirList.getItems().add(file);
                }
            }
        });

        dirList.getItems().addListener((ListChangeListener) change -> {
            if (dirList.getItems().isEmpty()) {
                scanButton.setDisable(true);
            } else {
                scanButton.setDisable(false);
            }
        });

        dirList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                removeDirButton.setDisable(false);
            } else {
                removeDirButton.setDisable(true);
            }
        });

        removeDirButton.setOnAction(actionEvent -> {
            dirList.getItems().remove(dirList.getSelectionModel().getSelectedItem());
        });

        scanButton.setOnAction(actionEvent -> {
            DirScanner dirScanner = new DirScanner();
            AtomicReference<String> doneMsgPart1 = new AtomicReference<>("");
            AtomicReference<String> doneMsgPart2 = new AtomicReference<>("");
            AtomicLong scannedSizeBytes = new AtomicLong();
            AtomicLong calculatedDigestBytes = new AtomicLong();
            dirScanner.scan(dirList.getItems(),
                    (file, size) -> {
                        scannedSizeBytes.set(size);
                        String readableSize = Utils.bytesIntoHumanReadable(size);
                        doneMsgPart2.set(readableSize);
                        Platform.runLater(() -> {
                            progressLabel.setText(doneMsgPart1.get() + doneMsgPart2.get());
                            scanningProgressBar.setProgress((double) calculatedDigestBytes.get() / (double) scannedSizeBytes.get());
                        });
                    }, (file, size) -> {
                        calculatedDigestBytes.set(size);
                        String readableSize = Utils.bytesIntoHumanReadable(size);
                        doneMsgPart1.set(readableSize + " (" + file.getName() + ")");
                        Platform.runLater(() -> {
                            progressLabel.setText(doneMsgPart1.get() + " of " + doneMsgPart2.get());
                            scanningProgressBar.setProgress((double) calculatedDigestBytes.get() / (double) scannedSizeBytes.get());
                        });
                    });
            Future<Object> submit = executorService.submit(() -> {
//                scan.getValue().get();
//                scan.getKey().get();
                return null;
            });
        });
    }


}
