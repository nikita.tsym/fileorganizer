package org.example;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;

public class DirScanner {

    private MessageDigest md;

    final byte[] buffer = new byte[1024 * 1024 * 16];

    private ExecutorService executorService = Executors.newCachedThreadPool();

    private AtomicLong calculatedSize = new AtomicLong();

    private AtomicReference<File> calculatedSizeObject = new AtomicReference<>();

    private Map<File, Long> calculatedSizeFiles = new HashMap<>();

    private Map<File, Long> calculatedDigestFiles = new HashMap<>();

    private AtomicLong calculatedDigestSize = new AtomicLong();


    public DirScanner() {
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public void scan(List<File> dirs,
                     BiConsumer<File, Long> sizeScanUpdate,
                     BiConsumer<File, Long> scanUpdate) {
        Future<Map<File, String>> scanning = executorService.submit(() -> {
            Map<File, String> hashes = new HashMap<>();
            long startSize = 0;
            for (File dir : dirs) {
                startSize = scan(dir, hashes, scanUpdate, startSize);
                //scanUpdate.accept(dir, 0L);
            }
            return hashes;
        });

        Future<Long> sizeScanning = executorService.submit(() -> {
            for (File dir : dirs) {
                Utils.getDirSize(dir, (file, size) -> {
                    calculatedSize.addAndGet(size);
                    calculatedSizeObject.set(file);
                    //scanUpdate.accept(calculatedSizeObject.get(), calculatedSize.get());
                    sizeScanUpdate.accept(calculatedSizeObject.get(), calculatedSize.get());
                    calculatedSizeFiles.put(file, size);
                });
            }
            return calculatedSize.get();
        });
    }

    private long scan(File dir, Map<File, String> hashes, BiConsumer<File, Long> scanUpdate, long startSize) {
        long processedSize = startSize;
        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                processedSize += scan(file, hashes, scanUpdate, processedSize);
            } else {
                try {
                    DigestInputStream digestInputStream = new DigestInputStream(new FileInputStream(file), md);

                    int num = 0;
                    do {
                        num = digestInputStream.read(buffer);
                        if (num > 0) {
                            processedSize = processedSize + num;
                            calculatedDigestSize.addAndGet(num);
                            scanUpdate.accept(file, calculatedDigestSize.get());
                        }
                        //System.out.println(processedSize * 100 / totalSize.get() + "%");
                    } while (num > 0);

                    String digest = byteArray2Hex(digestInputStream.getMessageDigest().digest());
                    hashes.put(file, digest);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return processedSize;
    }

    private static String byteArray2Hex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        return formatter.toString();
    }

}
